(function ($) {

    window.onload = function () {
        $(document).ready(function () {		
			back_to_top();
			slider_feed_back();
			slider_video();
			drop_moblie_links();
			open_nav_menu();
			fixed_header_bottom();
			slider_widget_one();
			open_search_hd();
        });        
    };
})(jQuery);



function fixed_header_bottom(){
	var header_bt = document.querySelector('#header .header-bottom');
	var check = true;
	window.addEventListener('scroll', function(){
		if(window.pageYOffset > 200){
			if(check == true){
				header_bt.classList.add("fixed-stuck");
				check = false;
			}			
		}
		else{
			if(check == false){
				header_bt.classList.remove("fixed-stuck");
				check = true;
			}
		}
	})
		
	
}

function back_to_top() {
	var offset = 300,
	//browser window scroll (in pixels) after which the "back to top" link opacity is reduced
	offset_opacity = 1200,
	//duration of the top scrolling animation (in ms)
	scroll_top_duration = 700,
	//grab the "back to top" link
	$back_to_top = $('.cd-top');
	//hide or show the "back to top" link
	$(window).scroll(function () {
		($(this).scrollTop() > offset) ? $back_to_top.addClass('cd-is-visible') : $back_to_top.removeClass('cd-is-visible cd-fade-out');
		if ($(this).scrollTop() > offset_opacity) {
			$back_to_top.addClass('cd-fade-out');
		}
	});
	//smooth scroll to top
	$back_to_top.on('click', function (event) {
		event.preventDefault();
		$('body,html').animate({
			scrollTop: 0,
		}, scroll_top_duration
	);
});

}

function slider_feed_back(){
	$('#home-page .client-feed-back .owl-carousel').owlCarousel({
		loop:true,
		margin:30,
		nav:false,
		dots: true,
		autoplay:true,
		autoplayTimeout:5000,
		autoplayHoverPause:true,
		responsiveClass:true,
		responsive:{
		0:{
			items:1,
		},
		576:{
		   items:3,
		   
		},
		768:{
		   items:2,
		},
		1024:{
			items:3,
		},

		1400:{
		   items:3,
		}
	}
  })
}

function slider_video(){
	$('#home-page .video-advice .owl-carousel').owlCarousel({
		loop:true,
		margin:30,
		nav:false,
		dots: true,
		autoplay:true,
		autoplayTimeout:5000,
		autoplayHoverPause:true,
		responsiveClass:true,
		responsive:{
		0:{
			items:1,
		},
		576:{
		   items:2,
		},
		768:{
		   items:3,
		},
		1024:{
			items:3,
		},

		1400:{
		   items:3,
		}
	 }
  })
}

function drop_moblie_links(){
	var has_drop_menu = $('#wrapper .moblie-menu-box .has-child-drop');
	var drop_list_mb = $('#wrapper .moblie-menu-box .has-child-drop .drop-down-mb');	
	if(has_drop_menu.length == 0){
		return 0;
	}
	else{
		has_drop_menu.append("<button class = 'btn-drop-nav-menu'><i class='fa fa-angle-right'></i></button>");
	}
	var btn_click_drop = $('.btn-drop-nav-menu');
	if(btn_click_drop.length == 0){
		return 0;
	}
	else{		
		for(var i = 0; i<btn_click_drop.length; i++){			
			btn_click_drop[i].onclick = function(){	
				this.classList.toggle("active");		
				for(k = 0; k <drop_list_mb.length; k++){
					var pannel = drop_list_mb[k].nextElementSibling;
					if(pannel.classList[1]  == "active"){
						drop_list_mb[k].classList.add("show-drop-down");
					}
					else{
						drop_list_mb[k].classList.remove("show-drop-down");
					}
				}
			}			
		}		
	}
}

function open_nav_menu(){
	var btn_open = document.querySelector('#wrapper #btn-open-nav-bar');
	var overlay = document.querySelector('#wrapper .over-lay-mb-bars');
	var close_nav_mb = document.querySelector('#btn-close-nav');
	var menu_mb = document.querySelector('#wrapper .moblie-menu-box');
	btn_open.onclick = function(){
		menu_mb.classList.add('open_mneu');
		overlay.classList.add('overlay-open');
	}

	close_nav_mb.onclick = function(){
		menu_mb.classList.remove('open_mneu');
		overlay.classList.remove('overlay-open');
	}

	overlay.onclick = function(){
		menu_mb.classList.remove('open_mneu');
		overlay.classList.remove('overlay-open');
	}
}

function slider_widget_one(){
	$('.widget-right .news-project .owl-carousel').owlCarousel({
		loop:true,
		margin:10,
		nav:false,
		dots: true,
		autoplay:true,
		autoplayTimeout:5000,
		autoplayHoverPause:true,
		responsiveClass:true,
		responsive:{
		0:{
			items:1,
		 },
		576:{
		    items:1,		   
		},
		768:{
		   items:1,
		},
		1024:{
			items:1,
		},

		1400:{
		   items:1,
		}
	 }
  })
}

function open_search_hd(){
	var btn_op_search = document.querySelectorAll('#wrapper .nav-link.search-header');
	var over_lay_search = document.querySelector('#wrapper .over-lay-mb');
	var box_search_hd = document.querySelector('#wrapper .box-search_header');
	for(var i = 0; i<btn_op_search.length; i++){
		btn_op_search[i].onclick = function(){
			over_lay_search.classList.add('overlay-open');
			box_search_hd.classList.add('active_show');
		}
	}
	over_lay_search.onclick = function(){
		over_lay_search.classList.remove('overlay-open');
		box_search_hd.classList.remove('active_show');
	}
}